//
//  VideosListView.swift
//  VIPER_Architechture_Task
//
//  Created by ArrowTN on 08/09/2017.
//  Copyright © 2017 ArrowTN. All rights reserved.
//

import UIKit

protocol VideosListProtocol: class {
    var presenter: VideosListPresenterProtocol? {get set}
    
    // PRESENTER -> VIEW
    func showVideos(with videos: [VideoModel])
    func showError()
    func showLoading()
    func hideLoading()
}

protocol VideosListPresenterProtocol: class {
    var view: VideosListProtocol? { get set }
    var interactor: VideosListInteractorInputProtocol? { get set }
    var wireFrame: VideosListWireFrameProtocol? { get set }
    
    // VIEW -> PRESENTER
    func viewDidLoad()
    func showVideoDetail(forVideo video: VideoModel)
}

protocol VideosListInteractorInputProtocol: class{
    var presenter: VideosListInteractorOutputProtocol? { get set }
    var localDatamanager: VideosListLocalDataManagerInputProtocol? { get set }
    var remoteDatamanager: VideosListRemoteDataManagerInputProtocol? { get set }
    
    // PRESENTER -> INTERACTOR
    func retrieveVideosList()
}

protocol VideosListInteractorOutputProtocol: class{
    // INTERACTOR -> PRESENTER
    func didRetrieveVideos(_ videos: [VideoModel])
    func onError()
}

protocol VideosListDataManagerInputProtocol: class {
    // INTERACTOR -> DATAMANAGER
}

protocol VideosListLocalDataManagerInputProtocol: class{
    // INTERACTOR -> LOCALDATAMANAGER
    func retrieveVideosList() throws -> [Video]
    func saveVideo(id: Int, name: String, eps: String, imgUrl: String, videoUrl: String, des: String, type: String, schedule: String) throws
}

protocol VideosListRemoteDataManagerInputProtocol: class {
    var remoteRequestHandler: VideosListRemoteDataManagerOutputProtocol? { get set }
    
    // INTERACTOR -> REMOTEDATAMANAGER
    func retrieveVideosList()
}

protocol VideosListRemoteDataManagerOutputProtocol: class {
    // REMOTEDATAMANAGER -> INTERACTOR
    func onVideosRetrieved(_ videos: [VideoModel])
    func onError()
}

protocol VideosListWireFrameProtocol: class {
    static func createVideosListModule() -> UIViewController
    // PRESENTER -> WIREFRAME
    func presentVideosDetailScreen(from view: VideosListProtocol, forVideo video: VideoModel)
}

//
//  ViewsListWireFrame.swift
//  VIPER_Architechture_Task
//
//  Created by ArrowTN on 11/09/2017.
//  Copyright © 2017 ArrowTN. All rights reserved.
//

import UIKit

class VideosListWireFrame: VideosListWireFrameProtocol{
    
    class func createVideosListModule() -> UIViewController {
        let navController = mainStoryboard.instantiateViewController(withIdentifier: "PostsNavigationController")
        if let view = navController.childViewControllers.first as? VideosListView {
            let presenter: VideosListPresenterProtocol & VideosListInteractorOutputProtocol = VideosListPresenter()
            let interactor: VideosListInteractorInputProtocol & VideosListRemoteDataManagerOutputProtocol = VideosListInteractor()
            let localDataManager: VideosListLocalDataManagerInputProtocol = VideosListLocalDataManager()
            let remoteDataManager: VideosListRemoteDataManagerInputProtocol = VideosListRemoteDataManager()
            let wireFrame: VideosListWireFrameProtocol = VideosListWireFrame()
            
            view.presenter = presenter
            presenter.view = view
            presenter.wireFrame = wireFrame
            presenter.interactor = interactor
            interactor.presenter = presenter
            interactor.localDatamanager = localDataManager
            interactor.remoteDatamanager = remoteDataManager
            remoteDataManager.remoteRequestHandler = interactor
            
            return navController
        }
        return UIViewController()
    }
    
    static var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
    
    func presentVideosDetailScreen(from view: VideosListProtocol, forVideo video: VideoModel){
        let videoDetailViewController = VideoDetailWireFrame.createVideoDetailModule(forVideo: video)
        if let sourceView = view as? UIViewController {
            sourceView.navigationController?.pushViewController(videoDetailViewController, animated: true)
        }
    }
}
